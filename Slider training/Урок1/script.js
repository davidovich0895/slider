//Задаем номер стартовой картинки, глобальные переменные
var slideIndex = 1;
var slides = document.querySelectorAll('.slide');
var dots = document.querySelector('.slider-dots');
// Вызыв основных функций
createDots();
showSlides();

//Создаю точки, число которых будет соответсвовать числу картинкок
function createDots(){
	for(let j = 0; j < slides.length; j++){
		var dot = document.createElement('span');
		dot.classList.add('slider-dots_item');
		dot.onclick = function(){
			showSlides(slideIndex= j+1);
		};
		dots.appendChild(dot);
	}
};

//Скрываем все картинки кроме первой
function showSlides(){
	if(slideIndex > slides.length){
		slideIndex = 1;
	} else if(slideIndex < 1){
		slideIndex = slides.length;
	}
	var dott = dots.children;
	for(let i = 0; i < slides.length; i++){
		slides[i].style.display = 'none';
		dott[i].classList.remove('active');
	}
	slides[slideIndex-1].style.display = 'block';
	dott[slideIndex-1].classList.add('active');
};

//Предыдущий слайд
function minusSlide(){
	showSlides(--slideIndex);
}

//Следующий слайд
function plusSlide(){
	showSlides(++slideIndex);
}


	



